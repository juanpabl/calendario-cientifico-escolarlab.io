BASE_URL = 'https://calendario-cientifico-escolar.gitlab.io'

year = args.length ? args[0] as int : Calendar.instance[Calendar.YEAR]

map = [
    'es':['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    'gal':['Xaneiro','Febreiro','Marzo/Marzal','Abril','Maio','Xuño','Xullo','Agosto','Setembro','Outubro','Novembro','Decembro'],
    'astu':['Xineru','Febreru','Marzu','Abril','Mayu','Xunu','Xunetu','Agostu','Setiembre','Ochobre','Payares','Avientu'],
    'eus':['Urtarril','Otsail','Martxo','Apiril','Maiatz','Ekain','Uztail','Abuztu','Irail','Urri','Azaro','Abendu'],
    'cat':['gener','febrer','març','abril','maig','juny','juliol','agost','setembre','octubre','novembre','desembre'],
    'arag':['Chinero','Febrero','Marzo','Abril','Mayo','Chunio','Chulio','Agosto','Setiembre','Octubre','Noviembre','Aviento'],
    'en':['January','February','March','April','May','June','July','August','September','October','November','December'],
    'epo':['Januaro','Februaro','Marto','Aprilo','Majo','Junio','Julio','Aŭgusto','Septembro','Oktobro','Novembro','Decembro'],
    'fra':['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
    'pt':['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro'],
    'arab':[
        "يناير",
        "فبراير",
        "مارس",
        "أبـــريل",
        "مايو",
        "يونــيــــو",
        "يوليو",
        "أغسطس",
        "سبتمــبـر",
        "أكتـوبر",
        "نوفمبر",
        "ديسمبر"
    ],
]

String altText( lang, year, month, date){ 
    String ret=""
    if( new File("source/csv/${year}/alttext.csv").exists() ){   
        new File("source/csv/${year}/alttext.csv").withReader{ r->
            r.readLine()
            def line
            while( (line=r.readLine())!= null){
                def fields = line.split(';')
                if( fields.length==3 && fields[0] as int == date && fields[1] as int == month){
                    ret = fields[2].replaceAll(","," ")
                }
            }
        }
    }
    return ret
}

map.each{ kv ->
    def lang = kv.key
    def months = kv.value
    
    def nav = new File("docs/${lang}/modules/ROOT/nav.adoc")
    nav.parentFile.mkdirs()
    months.eachWithIndex{ m,i->
        nav << "* xref:${i+1}.adoc[${m}]\n"
    }
    if( new File("source/csv/${year}/${lang}.tsv").exists() ){
        new File("source/csv/${year}/${lang}.tsv").withReader{ r->
            r.readLine()
            def line
            while( (line=r.readLine())!= null){
                def fields = line.split('\t')
                def title = fields[4].split('\\.').first()
                def body = fields[4].split('\\.').drop(1).join('.').trim()
                def month = fields[1] as int
                def day = fields[0] as int     
                def eventYear = lang != 'eus' ? title[-4..-1] : title[0..3]
                def file = new File("docs/${lang}/modules/ROOT/pages/${month}.adoc")
                file.parentFile.mkdirs()
                file << "[#${day}_${month}]\n"
                file << "== ${title}\n\n"
                file << "$body\n\n"
                if( new File("source/images/${year}/${fields[3]}.png").exists() ){
                    String alt = altText(lang, year, month, day)
                    file << "image::$BASE_URL/_/images/${year}/${fields[3]}.png[width=200,height=100,${alt ? 'alt='+alt:''}]\n\n"
                }
            }        
        }    
    }
}
