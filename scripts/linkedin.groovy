@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab(group='javax.mail', module='mail', version='1.4.7')

import static groovyx.net.http.MultipartContent.multipart
import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.ContentTypes.JSON
import groovyx.net.http.CoreEncoders
import groovyx.net.http.*
import static java.util.Calendar.*

year = args.length > 0 ? args[0] as int : new Date()[YEAR]
month = args.length > 1 ? args[1] as int : new Date()[MONTH]+1
day = args.length > 2 ? args[2] as int : new Date()[DAY_OF_MONTH]

println "Processing $year/$month/$day"

author=System.getenv("LINKEDIN_USER")
accessToken=System.getenv("LINKEDIN_TOKEN")

if( !author || !accessToken ){
    println "Necesito la configuracion de telegram"
    return
}

http = configure{
    request.uri = "https://api.linkedin.com"
    request.contentType = JSON[0]
    request.headers['Authorization'] = "Bearer $accessToken"
    request.headers['X-Restli-Protocol-Version'] = '2.0.0'
}

html = ""
img = ""

['es':'🇪🇸',
 'en':'🏴󠁧󠁢󠁥󠁮󠁧󠁿',
 'fra':'☫',
 'arab':'🇸🇦',
 'pt':'🇵🇹',
 'gal':'🐙',
 'astu':'🐮',
 'eus':'🪨',
 'cat':'🌊',
 'arag':'⛰️',
// 'epo':'🌍',
 ].each{ kv ->
    String lang = kv.key 
    String emoji = kv.value
    String[]found

    if( new File("source/csv/${year}/${lang}.tsv").exists() ){
        new File("source/csv/${year}/${lang}.tsv").withReader{ reader ->
            reader.readLine()
            String line
            while( (line=reader.readLine()) != null){
                def fields = line.split('\t')
                if( fields.length != 5)
                    continue
                if( fields[0] as int == day && fields[1] as int == month && fields[2] as int == year){
                    found = fields
                    break
                }
            }
        }
    }

    if(!found){
        println "not found $year/$month/$day"
        return
    }

    String title=  found[4].split('\\.').first()
	String body=  found[4].split('\\.').drop(1).join(' ')

    if( !img ){
        img = "https://calendario-cientifico-escolar.gitlab.io/_/images/${year}/${found[3]}.png"
    }

    html +="""
$emoji $title

$body

"""
}


html += """
Fuente: Calendario Cientifico Escolar, https://t.me/CalendarioCientifico
"""

json = http.post{
    request.uri.path = "/v2/assets"
    request.uri.query = ["action":"registerUpload"]
    request.body = [
        "registerUploadRequest": [
            "recipes": [
                "urn:li:digitalmediaRecipe:feedshare-image"
            ],
            "owner": "urn:li:person:"+author,
            "serviceRelationships": [
                [
                    "relationshipType": "OWNER",
                    "identifier": "urn:li:userGeneratedContent"
                ]
            ],
            "supportedUploadMechanism":[
                "SYNCHRONOUS_UPLOAD"
            ]
        ] 
    ]
}
uploadUrl = json.value.uploadMechanism["com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest"].uploadUrl

someFile = File.createTempFile("img","png")
someFile.bytes = img.toURL().bytes

configure{
    request.uri = uploadUrl
    request.contentType = 'image/png'
    request.headers['Authorization'] = "Bearer $accessToken"
    request.headers['X-Restli-Protocol-Version'] = '2.0.0'
    request.headers['Accept']='*/*'    
    request.body = someFile
    request.encoder('image/png'){ ChainedHttpConfig config, ToServer req->
        req.toServer(new ByteArrayInputStream(someFile.bytes))
    }    
}.put()


post = [
    "author": "urn:li:person:"+author,
    "lifecycleState":'PUBLISHED',
    "visibility": [
        "com.linkedin.ugc.MemberNetworkVisibility":'PUBLIC'
    ],
    "specificContent":[
        "com.linkedin.ugc.ShareContent":[
            "shareCommentary":[
                "text": html
            ],
            "shareMediaCategory" : "IMAGE",
            "media":[
                [status:'READY',media: json.value.asset]
            ]
        ]
    ]
]

http.post{
    request.uri.path = "/v2/ugcPosts"
    request.body = post
}
