import groovy.json.JsonOutput;

BASE_URL = 'https://calendario-cientifico-escolar.gitlab.io'

currentyear = args.length > 0 ? args[0] as int : new Date()[Calendar.YEAR]

[
    'es',
    'gal',
    'astu',
    'eus',
    'cat',
    'arag',
    'en',
    'epo',
    'fra',
    'arab',
    'pt',
].each{ lang ->
    if( new File("source/csv/${currentyear}/${lang}.tsv").exists() ){
        new File("source/csv/$currentyear/${lang}.tsv").withReader{ r->
            r.readLine()
            def line
            while( (line=r.readLine())!= null){
                def fields = line.split('\t')
                def title = fields[4].split('\\.').first()
                def body = fields[4].split('\\.').drop(1).join('.')
                def year = fields[2] as int
                def month = fields[1] as int
                def day = fields[0] as int     
                def eventYear = lang != 'eus' ? title[-4..-1] : title[0..3]
                def file = new File("public/api/${lang}/$year/$month/${day}.json")
                file.parentFile.mkdirs()
                file.text = JsonOutput.prettyPrint(JsonOutput.toJson([
                    lang:lang,
                    year:year,
                    month:month,
                    day:day,
                    title:title,
                    body:body,
                    eventYear:eventYear,
                    image: "$BASE_URL/_/images/${year}/${fields[3]}.png"
                ]))            
            }
        }    
    }
}
