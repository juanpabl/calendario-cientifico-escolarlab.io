= Un aniversario scientifico pa cada día de l'anyo.

Lo Calandario Scientifico Escolar ye endrezau mas que mas a l’alumnau d'educación primaria y secundaria obligatoria. Cada día se replega un aniversario scientifico u tecnolochico como, per eixemplo, naixencias de personas d'estes ambitos u commemoracions de troballas destacables.

Lo calandario s'acompanya d'una guía didactica con orientacions pa lo suyo aproveitamiento educativo transversal en as clases. Estas propuestas didacticas parten d'os prencipios d'inclusión, normalización y equidat y s'acompanyan de pautas chenerals d'accesibilidat. Pa ixo, se proporcionan quefers variaus que incluyen un amplo rango d'habilidatz y livels de dificultat y que, desenrolladas de manera cooperativa, permiten que tot l’alumnau faga aportacions utils y relevants. 

Pa favoreixer la suya difusión y emplego en as aulas, totz los materials son traducius a estas luengas: castellano, gallego, euskera, catalán, asturiano, aragonés, anglés, francés, esperanto y árabe.

La información d’os aniversarios diarios y las ilustracions que los acompanyan son disponibles en ubierto en este repositorio. Amás, se pueden descargar lo calandario y la guía (maquetaus en pdf y texto plano) dende la pachina web d'o IGM (http://www.igm.ule-csic.es/calendario-cientifico).

Esta iniciativa mira de contribuyir a acercar la cultura scientifica a la población mas choven y de crear referents lo mas cercanos posibles pa ells. Per ixo, s'ha feito un esfuerzo mayor en dar a conoixer personas y troballas d'o present que sían referencias pa los chovens y, de vez, den una visión de dinamismo y actualidat. S'ha feito especial atención a lo fomento d'un luengache no sexista y a l'aumento d'a visibilidat d'as mullers scientificas y tecnologas, pa meter a disposición modelos referents que promuevan las vocacions scientifico-tecnicas entre las ninas y adolescents. Tamién s'ha posau enfasi en divulgar l'actividat investigadera d'os centros publicos espanyols.

== Twitter

@CalCientifico

== Telegram

https://t.me/CalendarioCientifico

== iCal

link:{attachmentsdir}/arag.ical[Download]
