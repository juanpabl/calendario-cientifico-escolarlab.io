@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab('io.github.http-builder-ng:http-builder-ng-okhttp:1.0.4')

import static groovyx.net.http.OkHttpBuilder.configure
import groovyx.net.http.*
import static groovyx.net.http.ContentTypes.*
import static groovyx.net.http.MultipartContent.multipart

import static java.util.Calendar.*

year = args.length > 0 ? args[0] as int : new Date()[YEAR]
month = args.length > 1 ? args[1] as int : new Date()[MONTH]+1
day = args.length > 2 ? args[2] as int : new Date()[DAY_OF_MONTH]

println "Processing $year/$month/$day"

MASTODON_TOKEN=System.getenv("MASTODON_TOKEN")

if( !MASTODON_TOKEN ){
    println "Necesito la configuracion de mastodon"
    return
}

http = configure{
    request.uri = "https://botsin.space"
    request.contentType = JSON[0]
    request.encoder('image/png'){ ChainedHttpConfig config, ToServer req->
        req.toServer(new FileInputStream(file))
    }
}

['es':'🇪🇸',
 'en':'🏴󠁧󠁢󠁥󠁮󠁧󠁿',
 'fra':'🇫🇷',
 'arab':'🇸🇦',
 'pt':'🇵🇹',
 'gal':'🐙',
 'astu':'🐮',
 'eus':'🪨',
 'cat':'🌊',
 'arag':'⛰️',
 'epo':'🌍',
 ].each{ kv ->
    String lang = kv.key 
    String emoji = kv.value
    String[]found

    if( new File("source/csv/${year}/${lang}.tsv").exists() ){
        new File("source/csv/${year}/${lang}.tsv").withReader{ reader ->
            reader.readLine()
            String line
            while( (line=reader.readLine()) != null){
                def fields = line.split('\t')
                if( fields.length != 5)
                    continue
                if( fields[0] as int == day && fields[1] as int == month && fields[2] as int == year){
                    found = fields
                    break
                }
            }
        }
    }

    if(!found){
        println "not found $year/$month/$day"
        return
    }

    String title=  found[4].split('\\.').first()
	String body=  found[4].split('\\.').drop(1).join(' ')
    
    String msg ="""$emoji $title
$body
"""

    mediaToot = [:]
    file = new File("source/images/${year}/${found[3]}.png")
    if(file.exists()){
        mediaToot = http.post{
            request.uri = '/api/v1/media'
            request.headers.Authorization = "Bearer $MASTODON_TOKEN"
            request.contentType = 'multipart/form-data'
            request.body = multipart{
                part 'file', file.name, 'image/png', file
            }
            request.encoder 'multipart/form-data', OkHttpEncoders.&multipart
        }
    }
    
    
    toot = http.post{
        request.uri = '/api/v1/statuses'
        request.headers.Authorization = "Bearer $MASTODON_TOKEN"
        request.body = [
            status: msg
        ]
        if( mediaToot?.id )
            request.body.media_ids = [mediaToot.id]
    }


 }